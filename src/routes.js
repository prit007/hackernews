
import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Router} from 'react-router-dom';
import MainContent from './components/MainContent';

class Routes extends Component {
  render() {
    return (
    <BrowserRouter>
    <Router history={this.props.history}>
      <Switch>          
        <Route  path="/" component={MainContent} />
      </Switch>
    </Router>
    </BrowserRouter>     
    );
  }
}

export default Routes;
