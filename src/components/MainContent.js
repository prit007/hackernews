import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Posts from './Posts/Posts';
import TablePagination from '@material-ui/core/TablePagination';
import { history } from '../helpers/history';

const useStyles = makeStyles(theme => ({
  toolbar: theme.mixins.toolbar,
  title: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(3),
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    backgroundColor: theme.palette.background.default,
  },
  fullWidth: {
    width: '100%',
  },
}));

function MainContent() {
  const updateParam = (page, rows) => {
    history.push({
      search: `?page=${page}&rows=${rows}`
    })
  }

  const linkQuery = new URLSearchParams(window.location.search);
  const pageNumber = parseInt(linkQuery.get('page') || '0', 10);
  let rowsNumber = parseInt(linkQuery.get('rows') || '10', 10);
  if (rowsNumber > 100) {
    rowsNumber = 100;
    updateParam(pageNumber, rowsNumber);    
  } else if ([10, 25, 50, 100].indexOf(rowsNumber) === -1) {
    updateParam(pageNumber, 10)
  }
  const classes = useStyles();
  const [posts, setPosts] = React.useState([]);
  const [page, setPage] = React.useState(pageNumber);
  const [count, setCount] = React.useState(null);
  const [rowsPerPage, setRowsPerPage] = React.useState(rowsNumber);
  const [hiddenIds, setHiddenIds] = React.useState(JSON.parse(sessionStorage.getItem('hide'))  && JSON.parse(sessionStorage.getItem('hide')).length > 0 ? JSON.parse(sessionStorage.getItem('hide')) : []);

  const handleChangePage = (event, newPage) => {    
    event.preventDefault();
    setPage(newPage);    
    updateParam(newPage, rowsPerPage);
  };
  
  const handleChangeRowsPerPage = (event) => {
    event.preventDefault();
    const currentRowsPerPage = parseInt(event.target.value, 10);
    if (page > parseInt(count/currentRowsPerPage, 10)) {
      setPage(parseInt(count/currentRowsPerPage, 10)); 
      updateParam(parseInt(count/currentRowsPerPage, 10), currentRowsPerPage);
    } else {
      updateParam(page, parseInt(event.target.value, 10));
    }
    setRowsPerPage(currentRowsPerPage);    
  };

  const getHiddenIDs = (ids) => {
    if (ids && ids.length > 0) {
      setHiddenIds(ids);
    }
  };

  React.useEffect(() => {
    async function getTopStories() {
      setPosts([]);
      const url = "https://hacker-news.firebaseio.com/v0/topstories.json";
      try {
        const response = await fetch(url);
        if (response.ok === false) {
          throw new Error("Response Error:" + response.text);
        }
        const json = await response.json();
        if (JSON.parse(sessionStorage.getItem('hide')) && JSON.parse(sessionStorage.getItem('hide')).length > 0) {
          JSON.parse(sessionStorage.getItem('hide')).map((data, i) => json.splice(json.indexOf(JSON.parse(sessionStorage.getItem('hide'))[i]), 1))
        }        
        if (page > parseInt(json.length/rowsPerPage, 10)) {
          setPage(parseInt(json.length/rowsPerPage, 10)); 
          updateParam(parseInt(json.length/rowsPerPage, 10), rowsPerPage);
        };
        setCount(json.length);
        const promises = json
          .slice(rowsPerPage*page, rowsPerPage*(page+1))
          .map(id =>
            fetch(`https://hacker-news.firebaseio.com/v0/item/${id}.json`).then(
              response => response.json()
            )
          );
        const result = await Promise.all(promises);
        setPosts(result);
      } catch (err) {
        console.error(err);
      }
    }
    getTopStories();
  }, [page, rowsPerPage, hiddenIds]);


  return (
    <main className={classes.fullWidth}>
      <div className={classes.toolbar} />      
     {posts && count && <div className={classes.content}>       
            <Posts posts={posts} page={page} rowsPerPage={rowsPerPage} hiddenIds={ids => getHiddenIDs(ids)}/>
            <TablePagination
              component="div"
              count={count}
              page={page}
              onChangePage={handleChangePage}
              rowsPerPage={rowsPerPage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />

      </div> }
    </main>
  );
}

export default MainContent;