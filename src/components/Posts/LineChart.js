import React from "react";
import { LineChart, LineChartProps } from "@opd/g2plot-react";
import './LineChart.css'

let chartConfig = {
  title: {
    visible: true,
    text: "News Votes"
  },
  description: {
    visible: true,
    text: "News Votes Based on The News ID"
  },
  padding: "auto",
  forceFit: true,
  xField: "id",
  yField: "score",
  smooth: true,
  meta: {
    id: {
      alias: "id"
    },
    score: {
      alias: "score"
    }
  }
};


function LineCharts({ posts, page, rowsPerPage, hiddenIds, upvoteIds }) {
    const [chart, setChart] = React.useState(chartConfig);
    React.useMemo(() => {
        let postArray = [];
        function generateChart() {
            if (posts && posts.length > 0) {
                if (upvoteIds && upvoteIds.length >0) {
                    postArray =  posts.map((data, index) =>    {
                      return upvoteIds.findIndex(val =>val.id === data.id) > -1 ?
                      {id: data.id, score: data.score + upvoteIds[upvoteIds.findIndex(val =>val.id === data.id)].upvote} 
                      :{id: data.id, score: data.score}
                      
                    });
                } else {
                    postArray =  posts.map(res => {return  {id: res.id, score: res.score} })
                }  
                chartConfig.data = postArray;
                setChart(chartConfig)                
            }           
        }
        generateChart();
      }, [posts, upvoteIds]);

    return (
       <section className="chart-section">            
            <h2>News Line Chart</h2>
            {chart && <LineChart {...chart} /> 
            }
        </section> 
    )

}
export default LineCharts;
