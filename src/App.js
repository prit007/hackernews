import React from 'react';
import './App.css';
import TopMenu from './components/TopMenu';
import Routes from './routes';
import { history } from './helpers/history';

function App() {
  return (
    <div className="App">
      <TopMenu />
      <Routes history={history}/>
    </div>
  );
}

export default App;
